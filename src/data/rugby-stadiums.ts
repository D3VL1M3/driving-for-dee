import {StadiumsInterface} from "../types/interfaces/stadiums.interface";
import {SportsEnum} from "../types/enums/sports/sports.enum";

export const rugbyStadiums: StadiumsInterface = {
  type: SportsEnum.rugby,
  data: [
    {
      "stadium": "Ricoh Arena",
      "location": "Coventry",
      "visited": false
    },
    {
      "stadium": "Ashton Gate",
      "location": "Bristol",
      "visited": false
    },
    {
      "stadium": "Welford Road",
      "location": "Leicester",
      "visited": false
    },
    {
      "stadium": "Brentford Community Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Kingsholm Stadium",
      "location": "Gloucester",
      "visited": false
    },
    {
      "stadium": "Franklin's Gardens",
      "location": "Northampton",
      "visited": false
    },
    {
      "stadium": "Twickenham Stoop",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Recreation Ground",
      "location": "Bath",
      "visited": false
    },
    {
      "stadium": "Sandy Park",
      "location": "Exeter",
      "visited": false
    },
    {
      "stadium": "Sixways Stadium",
      "location": "Worcester",
      "visited": false
    },
    {
      "stadium": "AJ Bell Stadium",
      "location": "Salford",
      "visited": false
    },
    {
      "stadium": "Kingston Park",
      "location": "Newcastle",
      "visited": false
    }
  ]
}