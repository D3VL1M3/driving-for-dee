import {StadiumsInterface} from "../types/interfaces/stadiums.interface";
import {SportsEnum} from "../types/enums/sports/sports.enum";

export const footballStadiums: StadiumsInterface = {
  type: SportsEnum.football,
  data: [
    {
      "stadium": "Wembley Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Old Trafford",
      "location": "Manchester",
      "visited": false
    },
    {
      "stadium": "Tottenham Hotspur Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Emirates Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "London Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "City of Manchester Stadium",
      "location": "Manchester",
      "visited": false
    },
    {
      "stadium": "Anfield",
      "location": "Liverpool",
      "visited": false
    },
    {
      "stadium": "St James' Park",
      "location": "Newcastle upon Tyne",
      "visited": false
    },
    {
      "stadium": "Stadium of Light",
      "location": "Sunderland",
      "visited": false
    },
    {
      "stadium": "Villa Park",
      "location": "Birmingham",
      "visited": false
    },
    {
      "stadium": "Stamford Bridge",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Hillsborough Stadium",
      "location": "Sheffield",
      "visited": false
    },
    {
      "stadium": "Goodison Park",
      "location": "Liverpool",
      "visited": false
    },
    {
      "stadium": "Elland Road",
      "location": "Leeds",
      "visited": false
    },
    {
      "stadium": "Riverside Stadium",
      "location": "Middlesbrough",
      "visited": false
    },
    {
      "stadium": "Pride Park Stadium",
      "location": "Derby",
      "visited": false
    },
    {
      "stadium": "Bramall Lane",
      "location": "Sheffield",
      "visited": false
    },
    {
      "stadium": "Coventry Building Society Arena",
      "location": "Coventry",
      "visited": false
    },
    {
      "stadium": "St Mary's Stadium",
      "location": "Southampton",
      "visited": false
    },
    {
      "stadium": "King Power Stadium",
      "location": "Leicester",
      "visited": false
    },
    {
      "stadium": "Molineux",
      "location": "Wolverhampton",
      "visited": false
    },
    {
      "stadium": "Ewood Park",
      "location": "Blackburn",
      "visited": false
    },
    {
      "stadium": "Falmer Stadium",
      "location": "Brighton",
      "visited": false
    },
    {
      "stadium": "Stadium MK",
      "location": "Milton Keynes",
      "visited": false
    },
    {
      "stadium": "City Ground",
      "location": "Nottingham",
      "visited": false
    },
    {
      "stadium": "Portman Road",
      "location": "Ipswich",
      "visited": false
    },
    {
      "stadium": "bet365 Stadium",
      "location": "Stoke-on-Trent",
      "visited": false
    },
    {
      "stadium": "St Andrew's",
      "location": "Birmingham",
      "visited": false
    },
    {
      "stadium": "University of Bolton Stadium",
      "location": "Bolton",
      "visited": false
    },
    {
      "stadium": "Carrow Road",
      "location": "Norwich",
      "visited": false
    },
    {
      "stadium": "The Valley",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "The Hawthorns",
      "location": "West Bromwich",
      "visited": false
    },
    {
      "stadium": "Ashton Gate Stadium",
      "location": "Bristol",
      "visited": false
    },
    {
      "stadium": "Selhurst Park",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Craven Cottage",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "KCOM Stadium",
      "location": "Hull",
      "visited": false
    },
    {
      "stadium": "DW Stadium",
      "location": "Wigan",
      "visited": false
    },
    {
      "stadium": "Valley Parade",
      "location": "Bradford",
      "visited": false
    },
    {
      "stadium": "Madejski Stadium",
      "location": "Reading",
      "visited": false
    },
    {
      "stadium": "Kirklees Stadium",
      "location": "Huddersfield",
      "visited": false
    },
    {
      "stadium": "Deepdale",
      "location": "Preston",
      "visited": false
    },
    {
      "stadium": "Oakwell",
      "location": "Barnsley",
      "visited": false
    },
    {
      "stadium": "Vicarage Road",
      "location": "Watford",
      "visited": false
    },
    {
      "stadium": "Turf Moor",
      "location": "Burnley",
      "visited": false
    },
    {
      "stadium": "Fratton Park",
      "location": "Portsmouth",
      "visited": false
    },
    {
      "stadium": "The Den",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Meadow Lane",
      "location": "Nottingham",
      "visited": false
    },
    {
      "stadium": "Vale Park",
      "location": "Stoke-on-Trent",
      "visited": false
    },
    {
      "stadium": "Loftus Road",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Brunton Park",
      "location": "Carlisle",
      "visited": false
    },
    {
      "stadium": "Home Park",
      "location": "Plymouth",
      "visited": false
    },
    {
      "stadium": "Bloomfield Road",
      "location": "Blackpool",
      "visited": false
    },
    {
      "stadium": "Brentford Community Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Prenton Park",
      "location": "Birkenhead",
      "visited": false
    },
    {
      "stadium": "County Ground",
      "location": "Swindon",
      "visited": false
    },
    {
      "stadium": "London Road",
      "location": "Peterborough",
      "visited": false
    },
    {
      "stadium": "Keepmoat Stadium",
      "location": "Doncaster",
      "visited": false
    },
    {
      "stadium": "Boundary Park",
      "location": "Oldham",
      "visited": false
    },
    {
      "stadium": "Kassam Stadium",
      "location": "Oxford",
      "visited": false
    },
    {
      "stadium": "Roots Hall",
      "location": "Southend",
      "visited": false
    },
    {
      "stadium": "Memorial Stadium",
      "location": "Bristol",
      "visited": false
    },
    {
      "stadium": "New York Stadium",
      "location": "Rotherham",
      "visited": false
    },
    {
      "stadium": "Gateshead International Stadium",
      "location": "Gateshead",
      "visited": false
    },
    {
      "stadium": "Gigg Lane",
      "location": "Bury",
      "visited": false
    },
    {
      "stadium": "Priestfield Stadium",
      "location": "Gillingham",
      "visited": false
    },
    {
      "stadium": "Dean Court",
      "location": "Bournemouth",
      "visited": false
    },
    {
      "stadium": "Bescot Stadium",
      "location": "Walsall",
      "visited": false
    },
    {
      "stadium": "Edgeley Park",
      "location": "Stockport",
      "visited": false
    },
    {
      "stadium": "The Shay",
      "location": "Halifax",
      "visited": false
    },
    {
      "stadium": "Proact Stadium",
      "location": "Chesterfield",
      "visited": false
    },
    {
      "stadium": "Kenilworth Road",
      "location": "Luton",
      "visited": false
    },
    {
      "stadium": "Adams Park",
      "location": "Wycombe",
      "visited": false
    },
    {
      "stadium": "Spotland",
      "location": "Rochdale",
      "visited": false
    },
    {
      "stadium": "Sincil Bank",
      "location": "Lincoln",
      "visited": false
    },
    {
      "stadium": "Colchester Community Stadium",
      "location": "Colchester",
      "visited": false
    },
    {
      "stadium": "Alexandra Stadium",
      "location": "Crewe",
      "visited": false
    },
    {
      "stadium": "Field Mill",
      "location": "Mansfield",
      "visited": false
    },
    {
      "stadium": "New Meadow",
      "location": "Shrewsbury",
      "visited": false
    },
    {
      "stadium": "Huish Park",
      "location": "Yeovil",
      "visited": false
    },
    {
      "stadium": "Abbey Stadium",
      "location": "Cambridge",
      "visited": false
    },
    {
      "stadium": "Blundell Park",
      "location": "Cleethorpes",
      "visited": false
    },
    {
      "stadium": "Plough Lane",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Brisbane Road",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Glanford Park",
      "location": "Scunthorpe",
      "visited": false
    },
    {
      "stadium": "Twerton Park",
      "location": "Bath",
      "visited": false
    },
    {
      "stadium": "St James Park",
      "location": "Exeter",
      "visited": false
    },
    {
      "stadium": "The Walks",
      "location": "King's Lynn",
      "visited": false
    },
    {
      "stadium": "Bootham Crescent",
      "location": "York",
      "visited": false
    },
    {
      "stadium": "Victoria Park",
      "location": "Hartlepool",
      "visited": false
    },
    {
      "stadium": "Sixfields Stadium",
      "location": "Northampton",
      "visited": false
    },
    {
      "stadium": "Recreation Ground",
      "location": "Aldershot",
      "visited": false
    },
    {
      "stadium": "Whaddon Road",
      "location": "Cheltenham",
      "visited": false
    },
    {
      "stadium": "Academy Stadium",
      "location": "Manchester",
      "visited": false
    },
    {
      "stadium": "Broadhall Way",
      "location": "Stevenage",
      "visited": false
    },
    {
      "stadium": "Pirelli Stadium",
      "location": "Burton-upon-Trent",
      "visited": false
    },
    {
      "stadium": "York Street",
      "location": "Boston",
      "visited": false
    },
    {
      "stadium": "Plainmoor",
      "location": "Torquay",
      "visited": false
    },
    {
      "stadium": "Bower Fold",
      "location": "Stalybridge",
      "visited": false
    },
    {
      "stadium": "Globe Arena",
      "location": "Morecambe",
      "visited": false
    },
    {
      "stadium": "Moss Rose",
      "location": "Macclesfield",
      "visited": false
    },
    {
      "stadium": "New Bucks Head",
      "location": "Telford",
      "visited": false
    },
    {
      "stadium": "Aggborough",
      "location": "Kidderminster",
      "visited": false
    },
    {
      "stadium": "Moss Lane",
      "location": "Altrincham",
      "visited": false
    },
    {
      "stadium": "Keys Park",
      "location": "Hednesford Town",
      "visited": false
    },
    {
      "stadium": "Haig Avenue",
      "location": "Southport",
      "visited": false
    },
    {
      "stadium": "Victoria Road",
      "location": "Dagenham",
      "visited": false
    },
    {
      "stadium": "Kingfield Stadium",
      "location": "Woking",
      "visited": false
    },
    {
      "stadium": "Mill Farm",
      "location": "Kirkham",
      "visited": false
    },
    {
      "stadium": "The Camrose",
      "location": "Basingstoke",
      "visited": false
    },
    {
      "stadium": "Cherrywood Road",
      "location": "Farnborough",
      "visited": false
    },
    {
      "stadium": "Broadfield Stadium",
      "location": "Crawley",
      "visited": false
    },
    {
      "stadium": "Crabble Athletic Ground",
      "location": "Dover",
      "visited": false
    },
    {
      "stadium": "Damson Park",
      "location": "Solihull",
      "visited": false
    },
    {
      "stadium": "Highbury Stadium",
      "location": "Fleetwood",
      "visited": false
    },
    {
      "stadium": "West Leigh Park",
      "location": "Havant",
      "visited": false
    },
    {
      "stadium": "The Hive Stadium",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Silverlake Stadium",
      "location": "Eastleigh",
      "visited": false
    },
    {
      "stadium": "The New Lawn",
      "location": "Nailsworth",
      "visited": false
    },
    {
      "stadium": "Deva Stadium",
      "location": "Chester",
      "visited": false
    },
    {
      "stadium": "Moor Lane",
      "location": "Salford",
      "visited": false
    },
    {
      "stadium": "Wham Stadium",
      "location": "Accrington",
      "visited": false
    },
    {
      "stadium": "Holker Street",
      "location": "Barrow-in-Furness",
      "visited": false
    },
    {
      "stadium": "Gander Green Lane",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Stonebridge Road",
      "location": "Northfleet",
      "visited": false
    },
    {
      "stadium": "Fortress Stadium",
      "location": "Bromley",
      "visited": false
    }
  ]
}