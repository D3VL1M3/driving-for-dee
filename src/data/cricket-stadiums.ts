import {StadiumsInterface} from "../types/interfaces/stadiums.interface";
import {SportsEnum} from "../types/enums/sports/sports.enum";

export const cricketStadiums: StadiumsInterface = {
  type: SportsEnum.cricket,
  data: [
    {
      "stadium": "Riverside Ground",
      "location": "Chester-le-Street",
      "visited": true
    },
    {
      "stadium": "Rose Bowl",
      "location": "West End",
      "visited": false
    },
    {
      "stadium": "Lords",
      "location": "London",
      "visited": false
    },
    {
      "stadium": "Trent Bridge",
      "location": "West Bridgford",
      "visited": false
    },
    {
      "stadium": "Somerset County Ground",
      "location": "Taunton",
      "visited": false
    },
    {
      "stadium": "Sussex County Cricket Ground",
      "location": "Hove",
      "visited": false
    },
    {
      "stadium": "Edgbaston",
      "location": "Birmingham",
      "visited": false
    },
    {
      "stadium": "New Road",
      "location": "Worcester",
      "visited": false
    },
    {
      "stadium": "Headingley",
      "location": "Leeds",
      "visited": false
    },
    {
      "stadium": "Derbyshire County Ground",
      "location": "Derby",
      "visited": false
    },
    {
      "stadium": "County Cricket Ground",
      "location": "Chelmsford",
      "visited": false
    },
    {
      "stadium": "The Glamorgan County Cricket Ground",
      "location": "Cardiff",
      "visited": false
    },
    {
      "stadium": "Bristol County Cricket Ground",
      "location": "Bristol",
      "visited": false
    },
    {
      "stadium": "St Lawrence Ground",
      "location": "Canterbury",
      "visited": false
    },
    {
      "stadium": "Old Trafford",
      "location": "Manchester",
      "visited": false
    },
    {
      "stadium": "Grace Road",
      "location": "Leicester",
      "visited": false
    },
    {
      "stadium": "County Cricket Ground",
      "location": "Northampton",
      "visited": false
    },
    {
      "stadium": "The Oval",
      "location": "London",
      "visited": false
    }
  ]
}