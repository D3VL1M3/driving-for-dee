import {FunctionComponent, useMemo, useRef, useState} from 'react';

import {StadiumRowComponent, StadiumRowComponentProps} from './components/club-row/stadium-row.component';
import Icon from "@mdi/react";
import {mdiSort, mdiSortAscending, mdiSortDescending} from "@mdi/js";

import {sortByDirection} from "./utils/sort/sort-by-direction/sort-by-direction.util";

import {SportsEnum} from './types/enums/sports/sports.enum';
import {SortDir} from "./types/enums/sort-dir/sort-dir.enum";

import {StadiumsInterface} from './types/interfaces/stadiums.interface';

import {cricketStadiums} from './data/cricket-stadiums';
import {footballStadiums} from './data/football-stadiums';
import {rugbyStadiums} from './data/rugby-stadiums';

enum SortableIdentifier {
  stadium = 'stadium',
  location = 'location',
  visited = 'visited'
}

interface SortableHeaderProps {
  identifier: SortableIdentifier,
  label: string
}


const sortableHeaders: SortableHeaderProps[] = [
  {
    identifier: SortableIdentifier.stadium,
    label: 'Stadium'
  },
  {
    identifier: SortableIdentifier.location,
    label: 'Location'
  },
  {
    identifier: SortableIdentifier.visited,
    label: 'Visited'
  }
];

function mapStadiums(stadiums: StadiumsInterface, sport: SportsEnum): StadiumRowComponentProps[] {
  const mappedStadiums: StadiumRowComponentProps[] = [];

  stadiums.data.forEach((stadium) => {
    mappedStadiums.push({sport, ...stadium});
  })

  return mappedStadiums;
}


export const App: FunctionComponent = () => {
  const [sortBy, setSortBy] = useState(SortableIdentifier.visited);
  const [sortDirection, setSortDirection] = useState(SortDir.desc);
  const [searchTerm, setSearchTerm] = useState('');

  const textInput = useRef(null);

  const allStadiums = useMemo(() => [
    ...mapStadiums(cricketStadiums, SportsEnum.cricket),
    ...mapStadiums(footballStadiums, SportsEnum.football),
    ...mapStadiums(rugbyStadiums, SportsEnum.rugby),
  ], []);

  const sortedStadiums = useMemo((): StadiumRowComponentProps[] => {
    const sorted = [...allStadiums]
      .filter((a) => {
        for ( let key in a ) {
          if (
            key in SortableIdentifier
            && key !== SortableIdentifier.visited
            // @ts-ignore
            && a[key].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          ) return true;

        }
        return false;
      })
      .sort((a, b) => {
        const aSortBy = sortBy !== SortableIdentifier.visited ? a[sortBy].toLowerCase() : a[sortBy];
        const bSortBy = sortBy !== SortableIdentifier.visited ? b[sortBy].toLowerCase() : b[sortBy];

        return sortByDirection(aSortBy, bSortBy, sortDirection)
      });

    console.log({sorted});

    return sorted;

  }, [searchTerm, sortBy, sortDirection, allStadiums]);

  const visitedCount = allStadiums.filter((a) => a.visited).length;

  const sortColumns = (sortIdentifier: SortableIdentifier) => {
    if ( sortBy === sortIdentifier ) {
      setSortDirection(sortDirection === SortDir.asc ? SortDir.desc : SortDir.asc);
    } else {
      setSortDirection(SortDir.asc);
    }
    setSortBy(sortIdentifier);
  }

  const getSortIcon = (sortIdentifier: SortableIdentifier) => {
    const directionIcon = sortDirection === SortDir.asc ? mdiSortAscending : mdiSortDescending;
    const icon = sortBy === sortIdentifier ? directionIcon : mdiSort;
    return (<Icon path={icon} size={1} />);
  }

  return (
    <div className="container">
      <h1>🏍 Driving for Dee 🏍️</h1>
      <br />
      <h2>Visited Total: {visitedCount}/{allStadiums.length}</h2>

      <div className="input-group">
        <div className="form-outline">
          <label className="sr-only">Search</label>
          <input type="search" ref={textInput} onChange={(e) => setSearchTerm(e.target.value)} className="form-control" placeholder="search"/>
        </div>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            {sortableHeaders.map(({label, identifier}) => {
              const icon = getSortIcon(identifier);
              let classes = 'btn';

              classes += sortBy === identifier ? ' btn-success' : ' btn-primary';

              return (
                <th>
                  <button
                    className={classes}
                    onClick={() => sortColumns(identifier)}
                    type="button">
                    {label} {icon}
                  </button>
                </th>
              )
            })}
          </tr>
        </thead>
        <tbody>
          {sortedStadiums.map((club) => (<StadiumRowComponent {...club}/>))}
        </tbody>
      </table>
    </div>
  );
}
