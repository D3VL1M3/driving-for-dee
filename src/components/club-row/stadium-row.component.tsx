import Icon from '@mdi/react'
import { mdiSoccer } from '@mdi/js';
import { mdiRugby } from '@mdi/js';
import { mdiCricket } from '@mdi/js';
import { mdiCheckCircleOutline } from '@mdi/js';

import {SportsEnum} from "../../types/enums/sports/sports.enum";

export interface StadiumRowComponentProps {
  sport: SportsEnum,
  stadium: string;
  location: string;
  visited: boolean;
}

const sportIconReducer = (sport: SportsEnum) => {
  switch (sport) {
    case 'rugby' :
      return mdiRugby
    case 'cricket' :
      return mdiCricket
    case 'football' :
    default :
      return mdiSoccer
  }
}

export const StadiumRowComponent = ({
  sport = SportsEnum.football,
  stadium = '',
  location = '',
  visited = false,
} : StadiumRowComponentProps) => {

  const icon = sportIconReducer(sport);

  return (<>
    <tr>
        <td>
          <Icon path={icon}
                title={sport}
                size={2/3} /> {stadium}
        </td>
        <td>
          {location}
        </td>
        <td>
          {visited && (<Icon path={mdiCheckCircleOutline}
            title={sport}
            size={2/3}
            color="green"/>)}
        </td>
      </tr>
  </>)
}