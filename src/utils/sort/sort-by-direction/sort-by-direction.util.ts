import {SortDir} from "../../../types/enums/sort-dir/sort-dir.enum";

export const sortByDirection = (a: any, b: any, sortDir: SortDir = SortDir.asc) => {
  const mult = sortDir === SortDir.asc ? 1 : -1;
  // if neither have data, leave them
  if (a === undefined && b === undefined) {
    return 0;
  }

  if (a === undefined) {
    return -1 * mult;
  }

  if (b === undefined) {
    return 1 * mult;
  }

  if (a < b) {
    return -1 * mult;
  }
  if (a > b) {
    return 1 * mult;
  }
  return 0;
};
