import {SportsEnum} from "../enums/sports/sports.enum";
import {StadiumInterface} from "./stadium.interface";

export interface StadiumsInterface {
  type: SportsEnum,
  data: StadiumInterface[]
}