export interface StadiumInterface {
  stadium: string,
  location: string,
  visited: boolean,
}